#!/usr/bin/env bash
# shellcheck disable=SC2250

set -Eeuxo pipefail

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

HOST_ARCH="$(buildah info | jq '.host.arch' | tr -d '"')"
# ARCHS="amd64 arm64 ppc64le s390x"
ARCHS=${ARCHS:-$HOST_ARCH}

MANIFEST_NAME=${MANIFEST_NAME:-gitea}
WORKDIR=${WORKDIR:-/src}
GOVERSION=${GOVERSION:-1.18-bullseye}
USE_FEDORA_SCRATCH=${USE_FEDORA_SCRATCH:-false}

# shellcheck disable=SC2154
if [[ ${USE_TMPFS:-false} == true ]]; then
    tmp_dir="$(mktemp -d)"
    pushd "$tmp_dir"
fi

# shellcheck disable=SC2154
if [[ ${GITLAB_CI:-false} != true ]]; then
    git clone https://github.com/go-gitea/gitea
    if [[ -v PROJECT_REF ]]; then
        pushd gitea
        git checkout "$PROJECT_REF"
        popd
    fi

    buildah manifest rm "$MANIFEST_NAME" 2>/dev/null || true
    buildah manifest create "$MANIFEST_NAME"

    source "${SCRIPTPATH}/go_cache.sh"
fi

GOPROXY=${GOPROXY:-direct}
TAGS="bindata timetzdata ${TAGS:-sqlite sqlite_unlock_notify}"
CGO_EXTRA_CFLAGS=${CGO_EXTRA_CFLAGS:-""}



for ARCH in $ARCHS; do
    case $ARCH in

        "$HOST_ARCH")
          unset -v DPKG_ADD_ARCH
          GCC_PACKAGES="gcc libc6-dev"
          CC="gcc"
          DNF_FORCEARCH=""
          ;;

#        386)
#          DPKG_ADD_ARCH="i386"
#          GCC_PACKAGES="gcc-i686-linux-gnu libc6-dev-i386-cross"
#          CC="i686-linux-gnu-gcc"
#          ;;

        amd64)
          DPKG_ADD_ARCH="amd64"
          GCC_PACKAGES="gcc-x86-64-linux-gnu libc6-dev-amd64-cross"
          CC="x86_64-linux-gnu-gcc"
          DNF_FORCEARCH="--forcearch x86_64"
          ;;

#        arm)
#          DPKG_ADD_ARCH="armhf"
#          GCC_PACKAGES="gcc-arm-linux-gnueabihf libc6-dev-armhf-cross"
#          CC="arm-linux-gnueabihf-gcc"
#          ;;

        arm64)
          DPKG_ADD_ARCH="arm64"
          GCC_PACKAGES="gcc-aarch64-linux-gnu libc6-dev-arm64-cross"
          CC="aarch64-linux-gnu-gcc"
          DNF_FORCEARCH="--forcearch aarch64"
          ;;

        ppc64le)
          DPKG_ADD_ARCH="ppc64el"
          GCC_PACKAGES="gcc-powerpc64le-linux-gnu libc6-dev-ppc64el-cross"
          CC="powerpc64le-linux-gnu-gcc"
          DNF_FORCEARCH="--forcearch ppc64le"
          ;;

        s390x)
          DPKG_ADD_ARCH="s390x"
          GCC_PACKAGES="gcc-s390x-linux-gnu libc6-dev-s390x-cross"
          CC="s390x-linux-gnu-gcc"
          DNF_FORCEARCH="--forcearch s390x"
          ;;

        *)
          echo "Unsupported architecture!" 1>&2
          exit 1
          ;;

    esac


    ctr_builder=$(buildah from "docker.io/library/golang:${GOVERSION}")
    buildah copy "$ctr_builder" gitea "$WORKDIR"
    #buildah copy "$ctr_builder" go /go
    test -v DPKG_ADD_ARCH && buildah run "$ctr_builder" -- dpkg --add-architecture "$DPKG_ADD_ARCH"
    buildah run "$ctr_builder" -- apt-get update
    buildah run "$ctr_builder" -- apt-get install -y --no-install-recommends curl
    buildah run "$ctr_builder" -- bash -c "curl -fsSL https://deb.nodesource.com/setup_18.x | bash -"
    # shellcheck disable=SC2086
    buildah run "$ctr_builder" -- apt-get install -y --no-install-recommends build-essential git nodejs $GCC_PACKAGES
    buildah run --workingdir "$WORKDIR" --env GOARCH="$ARCH" --env CC="$CC" --env TAGS="$TAGS" --env CGO_ENABLED=1 --env CGO_EXTRA_CFLAGS="$CGO_EXTRA_CFLAGS" "$ctr_builder" -- /bin/sh -c "make clean-all build && go build contrib/environment-to-ini/environment-to-ini.go"


    dnf_packages="bash ca-certificates curl gettext git gnupg2 nmap-ncat socat"
    if [[ $USE_FEDORA_SCRATCH == true ]]; then
        ctr=$(buildah from --arch "$ARCH" scratch)

        dnf_packages="glibc-minimal-langpack $dnf_packages"
        releasever="$(rpm -E %fedora)"

        if [[ $UID -ne 0 ]]; then
            buildah unshare sh -c "dnf -y --releasever=$releasever --nodocs --setopt install_weak_deps=False $DNF_FORCEARCH --installroot \"\$(buildah mount $ctr)\" install $dnf_packages" && buildah unmount "$ctr"
            buildah unshare sh -c "dnf -y --releasever=$releasever $DNF_FORCEARCH --installroot \"\$(buildah mount $ctr)\" clean all" && buildah unmount "$ctr"
        else
            ctr_mountpoint="$(buildah mount "$ctr")"
            # shellcheck disable=SC2086
            dnf -y --releasever="$releasever" --nodocs --setopt install_weak_deps=False $DNF_FORCEARCH --installroot "$ctr_mountpoint" install $dnf_packages
            # shellcheck disable=SC2086
            dnf -y --releasever="$releasever" $DNF_FORCEARCH --installroot "$ctr_mountpoint" clean all
            buildah unmount "$ctr"
        fi
    else
        ctr=$(buildah from --arch "$ARCH" registry.fedoraproject.org/fedora-minimal)

        # shellcheck disable=SC2086
        buildah run "$ctr" -- microdnf -y --nodocs --setopt install_weak_deps=0 install $dnf_packages
        buildah run "$ctr" -- microdnf -y clean all
    fi

    buildah run "$ctr" -- groupadd -r -g 1000 git
    buildah run "$ctr" -- useradd -r -g git -M -d /var/lib/gitea/git -s /bin/bash -u 1000 git
    
    buildah run "$ctr" -- mkdir -p /var/lib/gitea /etc/gitea
    buildah run "$ctr" -- chown git:git /var/lib/gitea /etc/gitea
    
    buildah copy "$ctr" gitea/docker/rootless /
    buildah copy --from "$ctr_builder" --chown=root:root "$ctr" "${WORKDIR}/gitea" /app/gitea/gitea
    buildah copy --from "$ctr_builder" --chown=root:root "$ctr" "${WORKDIR}/environment-to-ini" /usr/local/bin/environment-to-ini
    buildah run "$ctr" -- chmod 755 /usr/local/bin/docker-entrypoint.sh /usr/local/bin/docker-setup.sh /app/gitea/gitea /usr/local/bin/gitea /usr/local/bin/environment-to-ini
    
    buildah copy "$ctr" "${SCRIPTPATH}/podman-health-notifier" /usr/local/bin/podman-health-notifier
    buildah run "$ctr" -- sed -i '/^#\!\/bin\/sh$/a exec /usr/local/bin/podman-health-notifier &' /usr/local/bin/docker-entrypoint.sh

    buildah config --arch "$ARCH" --label maintainer="dev@shdwchn.io" --user 1000:1000 \
                   --env GITEA_WORK_DIR=/var/lib/gitea --env GITEA_CUSTOM=/var/lib/gitea/custom --env GITEA_TEMP=/tmp/gitea \
                   --env TMPDIR=/tmp/gitea --env GITEA_APP_INI=/etc/gitea/app.ini --env HOME=/var/lib/gitea/git \
                   --port 2222 --port 3000 --volume /var/lib/gitea --volume /etc/gitea \
                   --entrypoint /usr/local/bin/docker-entrypoint.sh "$ctr"


    if [[ ${GITLAB_CI:-false} != true ]]; then
        buildah commit --manifest "$MANIFEST_NAME" "$ctr"
    else
        buildah commit "$ctr" "${MANIFEST_NAME}-${ARCH}"
    fi

    buildah rm "$ctr_builder"
    buildah rm "$ctr"
done



if [[ ${USE_TMPFS:-false} == true ]]; then
    popd
fi
