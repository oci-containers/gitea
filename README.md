# Unofficial gitea OCI container
## About
This repo is used to build [gitea](https://github.com/go-gitea/gitea) OCI container with sdnotify support for podman automatic updates and rollback. You can find built containers on [quay](https://quay.io/repository/oci-containers/gitea). Following architectures supported: amd64, aarch64, ppc64le and s390x.

Made by [shadowchain](https://gitlab.com/shdwchn10). Licensed under [MIT License](/LICENSE).

## How to use
1. Pull the container image from the user on which it will be run: `podman pull quay.io/oci-containers/gitea:latest`.
1. Place [gitea-podman.service](/gitea-podman.service) file to `~/.config/systemd/user/` if you want rootless container or to `/etc/systemd/system/` if you want run container as root.
1. Change `CHANGE_DATA_VOLUME` in the service file to your host volume (where the gitea data is stored).
1. Change `CHANGE_CFG_VOLUME` in the service file to your host volume (where the gitea config is stored).
1. Change `CHANGE_HOSTNAME` in the service file to a preferable container hostname.
1. Change ports configuration in the service file if you want to. Default web port — 3000, ssh port — 2222.
1. Reload systemd: `systemctl --user daemon-reload` for a rootless container or `systemctl daemon-reload` for a rootful container.
1. After that you can enable and run the container: `systemctl --user enable --now gitea-podman.service` or `systemctl enable --now gitea-podman.service` respectively.
1. If you want to enable automatic updates: `systemctl --user enable --now podman-auto-update.timer` or `systemctl enable --now podman-auto-update.timer`. If the automatic update fails, podman will rollback the problematic update.

## How to build locally
### Install dependencies
**Fedora Silverblue** already has required packages.

**Fedora/RHEL**:
```
sudo dnf install buildah git
```

**Ubuntu/Debian**:
```
sudo apt install buildah git
```

**Arch Linux**:
```
sudo pacman -S buildah git
```

### Build
Just run [build.sh](/build.sh). You can set some environment variables to change script behavior:

- `ARCHS` — build target architectures. If `ARCHS` is not set, the script builds container image **only for the host CPU architecture**.
- Set `USE_TMPFS` to `true` to build in `/tmp`. Otherwise the script will build the image **in the current directory**.
- Set `USE_FEDORA_SCRATCH` to `true` to build a container image from scratch (your host system must be Fedora with dnf). Otherwise the `fedora-minimal` base image will be used.

Example:
```
USE_FEDORA_SCRATCH=true USE_TMPFS=true ARCHS="amd64 arm64 ppc64le s390x" ./build.sh
```

## TODO
- Container CI testing
- More DRY
- Maybe more container tags on quay
- Find an easy way to automagically trigger CI on new gitea releases
